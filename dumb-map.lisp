;;;; dumb-map.lisp

(in-package #:dumb-map)


;;;; not the most effient way to make a map, but hey this is for fun right?

(defclass dumb-map ()
    ((key-fun
      :initarg :key-fun
      :accessor key-fun)
     (data
      :initform NIL
      :accessor data)))


(defmethod len ((map dumb-map)) (length (data map)))

(defmethod add-to-map-no-check ((map dumb-map) key val)
  (push (list :key key :val val) (data map)))

(defmethod keys ((map dumb-map)) (mapcar (lambda (x) (getf x :key)) (data map)))

(defmethod map-vals ((map dumb-map)) (mapcar (lambda (x) (getf x :val)) (data map)))

(defmethod iter ((map dumb-map)) (data map))


(defmethod in-map ((map dumb-map) key)
  (member key (keys map) :test (key-fun map)))

(defmethod get-val ((map dumb-map) key)
  (let ((pair (get-pair map key)))
    (if pair (getf pair :val) NIL)))

(defmethod get-index ((map dumb-map) key)
  (let ((map-size (len map))
	(tail-sub-list-size (length (in-map map key))))
    (if tail-sub-list-size
	(- map-size tail-sub-list-size)
	-1)))

(defmethod get-pair ((map dumb-map) key)
  (let ((index (get-index map key)))
    (if (>= index 0) (nth index(data map)) NIL)))

(defmethod clear ((map dumb-map)) (setf (data map) NIL))

(defmethod pop-key ((map dumb-map) key)
  (let ((old-pair (get-pair key))
	(index (get-index map key)))
    (if old-pair
	(if (and (not (zerop index)) (>= index (length (data map))))
	      (setf (nthcdr index (data map)) (nthcdr (1+ index) (data map)))))
    old-pair))


(defmethod update ((map dumb-map) key value)
  (let ((res NIL) (old-pair (get-pair map key)))
    (if old-pair
	(progn
	  (setf res (getf old-pair :val))
	  (setf (getf old-pair :val) value))
	(push (list :key key :val value) (data map)))
    res))
